import { useEffect } from "react";
import { useSelector } from 'react-redux';
import CreateRoom from "./components/CreateRoom";
import InRoom from "./components/InRoom";
import { RootState } from "./store/store";

function App() {

  const setting = useSelector((state: RootState) => state.settingSlice)
  const appState = useSelector((state: RootState) => state.appSlice)

  useEffect(() => {
    localStorage.setItem('bantron', JSON.stringify(appState))
  }, [appState]);


  switch (setting.step) {
    // state = 1 is create or join room
    case 1:
      return <CreateRoom />
    // state = 2 is joined room
    case 2:
      return <InRoom />
    default:
      return (
        <>
          <CreateRoom />
        </>
      )
  }
}

export default App
