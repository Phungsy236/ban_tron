import { appSlice } from './appSlice';
import { configureStore } from '@reduxjs/toolkit'
import { settingSlice } from './settingSlice';

export const store = configureStore({
  reducer: {
    appSlice : appSlice.reducer,
    settingSlice : settingSlice.reducer
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch