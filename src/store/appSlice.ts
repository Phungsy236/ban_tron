import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'
import { v4 as uuidv4 } from 'uuid';

type Member = {
    id: React.Key,
    name: string,
    balance: number,
    is_active: boolean,
    is_host: boolean
}

export interface appState {
    room_id: React.Key | null,
    countGame: 0,
    roomName: string | null,
    member: Member[] | [],
    isActive: boolean
    // settings : {
    //     minBet : number,
    //     k_donghoa : number,
    //     k_dongchat: number,
    //     k_10 : number
    // }
}

const initialState: appState[] | [] = JSON.parse(localStorage.getItem('bantron') || "[]")

export const appSlice = createSlice({
    name: 'app',
    initialState,
    reducers: {
        createRoom(state, action: PayloadAction<string>) {
            //@ts-ignore
            state.push({ room_id: uuidv4(), roomName: action.payload, member: [], isActive: false, countGame: 0 })
        },
        chooseRoom(state, action: PayloadAction<React.Key>) {
            state.forEach(item => item.isActive = false)
            const room = state.find(item => item.room_id === action.payload)
            if (room) {
                room.isActive = true
            }
        },

        addMember(state, action: PayloadAction<{ room_id: React.Key, name: string, is_host: boolean }>) {
            const member: Member = {
                id: uuidv4(),
                balance: 0,
                is_host: action.payload.is_host,
                name: action.payload.name,
                is_active: true
            }
            const idxRoom = state.findIndex(room => room.room_id === action.payload.room_id)
            if (member.is_host) {
                state[idxRoom].member.forEach(member => member.is_host = false)
            }
            state[idxRoom].member = [...state[idxRoom].member, member];

        },
        changeStatusMember(state, action: PayloadAction<{ member_id: React.Key, status: boolean }>) {
            const idxRoom = state.findIndex(room => room.isActive)
            const memberId = state[idxRoom].member.findIndex(mem => mem.id === action.payload.member_id);
            state[idxRoom].member[memberId].is_active = action.payload.status
        },
        updateBalance(state, action: PayloadAction<{ member_id: React.Key, amount: number }[]>) {
            const idxRoom = state.findIndex(room => room.isActive)
            const hostIdx = state[idxRoom].member.findIndex(mem => mem.is_host)
            action.payload.forEach(update => {
                const memIdx =
                    state[idxRoom].member.findIndex(mem => mem.id === update.member_id)

                state[idxRoom].member[memIdx].balance += update.amount
                state[idxRoom].member[hostIdx].balance -= update.amount
            })
            state[idxRoom].countGame += 1;
        },
        changeHost(state, action: PayloadAction<{ newId: React.Key }>) {
            const idxRoom = state.findIndex(room => room.isActive)
            state[idxRoom].member.forEach(mem => mem.is_host = false)
            const memHost = state[idxRoom].member.find(mem => mem.id === action.payload.newId)
            if (memHost) {
                memHost.is_host = true
            }

        }
    },
})

// Action creators are generated for each case reducer function
export const { addMember, changeStatusMember, updateBalance, changeHost, createRoom, chooseRoom } = appSlice.actions
