import { createSlice } from '@reduxjs/toolkit'
import type { PayloadAction } from '@reduxjs/toolkit'



export interface appState {
        step : number,
        minBet? : number,
        k_donghoa? : number,
        k_dongchat?: number,
        k_10? : number
    
}

const initialState: appState = {
    step : 1 , 
    
}

export const settingSlice = createSlice({
    name: 'app',
    initialState,
    reducers: {
        setStep(state, action: PayloadAction<number>) {
            state.step = action.payload 
        },
       
    },
})

// Action creators are generated for each case reducer function
export const { setStep } = settingSlice.actions
