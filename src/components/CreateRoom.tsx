import  { useState } from 'react'
import Modal from 'react-responsive-modal'
import { createRoom, appState, chooseRoom } from '../store/appSlice'
import { setStep } from '../store/settingSlice'
import { useDispatch, useSelector } from 'react-redux'
import { RootState } from '../store/store'

type Props = {}

export default function CreateRoom({ }: Props) {
    const dispatch = useDispatch()
    const appState = useSelector((state: RootState) => state.appSlice)
    const setting = useSelector((state: RootState) => state.settingSlice)
    const [roomName, setRoomName] = useState('')
    const [openModal, setOpenModal] = useState(false)

    function handleAddRoom() {
        if (roomName) {
            dispatch(createRoom(roomName))
            setOpenModal(false)
        }
    }
    function handleChooseRoom(room: appState) {
        dispatch(setStep(2))
        dispatch(chooseRoom(room.room_id!))
        // setStep(2)
        // setCurrentRoom(room)
    }
    return <>
        <div className="text-center p-6 h-screen w-screen">
            <div className="max-h-[100vh-200]">
                {appState.map(item => <div key={item.room_id} className="p-6 m-4 font-bold rounded-md shadow border cursor-pointer text-lg" onClick={() => handleChooseRoom(item)}>
                    {item.roomName}
                    <div className="text-slate-500 text-xs">({item.member.length} members)</div>
                </div>)}
            </div>

            {/* <button className="btn btn-primary">Join room</button> */}
            <button className="btn btn-danger" onClick={() => setOpenModal(true)}>Create new</button>
        </div>
        <Modal open={setting.step === 1 && openModal} onClose={() => setOpenModal(false)} center>
            <div className="m-6">
                <h2 className="font-bold text-xl mb-2 text-center">Create new room</h2>
                <input className="form-input rounded-lg" onChange={e => setRoomName(e.target.value)}></input>
                <div className="text-center m-2">
                    <button className="btn btn-primary" disabled={!roomName} onClick={handleAddRoom}>Create</button>
                </div>
            </div>
        </Modal>
    </>
}