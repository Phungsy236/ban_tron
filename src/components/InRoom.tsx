import React, {  useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Modal } from 'react-responsive-modal';
import { toast } from 'react-toastify';
import HandRaise from '../assets/HandRaise';
import HostIcon from '../assets/HostIcon';
import PlayIcon from '../assets/PlayIcon';
import PlayPause from '../assets/PlayPause';
import BackIcon from '../assets/BackIcon';
// import musicFile from '../assets/gamemusic.wav';
import { addMember, changeHost, changeStatusMember, updateBalance } from '../store/appSlice';
import { setStep } from '../store/settingSlice';
import { RootState } from '../store/store';
import Currentcy from './Currentcy';
type Props = {}

export default function InRoom({ }: Props) {
    const dispatch = useDispatch()
    const appState = useSelector((state: RootState) => state.appSlice)
    const setting = useSelector((state: RootState) => state.settingSlice)
    const [openModalSetting, setOpenModalSetting] = useState(false)
    const [member, setMember] = useState<{ name: string, is_host: boolean }>({ name: '', is_host: false })
    const [isStartGame, setStartGame] = useState(false);
    // const audioRef = useRef<HTMLAudioElement>(null)
    const [showModalUpdateBalance, setShowModalUpdateBalance] = useState(false)
    const [updateChangeSt, setUpdateChangeSt] = useState<{ member_id: React.Key, amount: number }[] | []>([])
    const currentRoom = appState.find(item => item.isActive)




    function handleAddMember() {
        if (member.name) {
            const checkDuplicate = currentRoom?.member.some(mem => mem.name === member.name);
            if (checkDuplicate) {
                toast.error("This name is already exist")
            } else {
                dispatch(addMember({ name: member.name, is_host: member.is_host, room_id: currentRoom!.room_id! }))
                setMember({ name: '', is_host: false })
            }
        }
    }
    function handleBack() {
        dispatch(setStep(1))
    }
    function handlePlayPause() {
        if (currentRoom?.member.length === 0) {
            toast.error("Them nguoi choi di bro!")
            return
        }
        if (!currentRoom?.member.some(mem => mem.is_host)) {
            toast.error("Chon host di bro!")
            return

        }
        if (isStartGame) {
            setShowModalUpdateBalance(true)
            setStartGame(false)
            // if (audioRef.current) {
            //     audioRef.current.pause()
            // }
        } else {
            setUpdateChangeSt([])
            setStartGame(true)
            // if (audioRef.current) {
            //     audioRef.current.play()
            // }
        }

    }
    function handleUpdateBalance() {
        if (updateChangeSt.length < currentRoom!.member.filter(mem => mem.is_active && !mem.is_host).length) {
            toast.error("Hmm , chua update ")
        }
        else {
            dispatch(updateBalance(updateChangeSt))
            setShowModalUpdateBalance(false)
            handlePlayPause()
        }
    }

    if (!currentRoom) return <></>
    return (
        <>
            <div className="text-center p-6 h-screen w-screen">
                <div className="flex justify-between" >
                    <div className="font-bold text-xl">
                        <div onClick={handleBack} >

                            <BackIcon />
                        </div>
                        {currentRoom?.roomName} <span className="text-xs text-slate-400">({currentRoom?.member.length} member - {currentRoom?.countGame} games played)  </span></div>
                    <button className="btn btn-primary" onClick={() => setOpenModalSetting(true)}>Settings</button>
                </div>
                <div className='mt-8'>
                    {currentRoom.member.length === 0 && <div className='flex justify-center text-2xl text-slate-400'>
                        <div><HandRaise /></div>
                        Click setting to add member
                    </div>}
                    {currentRoom?.member.map(mem => <div key={mem.id} className='p-4 my-3 border rounded-lg flex justify-between items-center'>
                        <div className='font-bold flex gap-2'>{mem.name} <span>{mem.is_host && <HostIcon />}</span></div>
                        <Currentcy balance={mem.balance} />
                    </div>)}
                </div>

                <button className='rounded-full fixed bottom-1 left-1/2 -translate-x-1/2 btn btn-primary p-6 ' onClick={handlePlayPause}>{isStartGame ? <PlayPause /> : <PlayIcon />}</button>
                {/* <audio controls className='hidden' src={musicFile} ref={audioRef} loop /> */}
            </div >
            <Modal open={setting.step === 2 && openModalSetting} onClose={() => setOpenModalSetting(false)} center>
                <div className="m-6">
                    <h2 className="font-bold text-xl mb-2 text-center">Members</h2>
                    <div>
                        {currentRoom?.member.map(mem => <div key={mem.id} className='p-2 my-2 border rounded-lg flex justify-between items-center'>
                            <div className='font-bold'>{mem.name}</div>
                            <div className='text-xs'>Active <input type="checkbox" checked={mem.is_active} onChange={(e) => dispatch(changeStatusMember({ member_id: mem.id, status: e.target.checked }))} /></div>
                            <div className='text-xs'>Host <input type="checkbox" checked={mem.is_host}
                                onChange={() => dispatch(changeHost({ newId: mem.id }))} /></div>
                        </div>)}
                    </div>

                    <hr className="mb-4"></hr>
                    <div className="flex gap-8 items-center flex-wrap">
                        <span>Name:</span>
                        <input className="form-input rounded-lg" maxLength={12} id="name" onChange={e => setMember(member => ({ ...member, name: e.target.value }))} value={member.name}></input>
                        <span>
                            <input type="checkbox" id="host" className="form-checkbox" checked={member.is_host}
                                onChange={e => setMember(member => ({ ...member, is_host: e.target.checked }))}
                            /> Host
                        </span>
                        <button className="btn btn-danger" onClick={handleAddMember}>Add</button>
                    </div>
                </div>
                <div className='text-center'>
                    <button className='btn btn-primary' onClick={() => setOpenModalSetting(false)}>Close</button>
                </div>
            </Modal>

            <Modal open={setting.step === 2 && showModalUpdateBalance} onClose={() => {
                setShowModalUpdateBalance(false)
                handlePlayPause()
            }} center>
                <div className="m-6">
                    <h2 className="font-bold text-xl mb-2 text-center">Chia tien</h2>
                    <div>
                        {currentRoom?.member.filter(mem => !mem.is_host).map(mem =>
                            <div key={mem.id} className='p-2 my-2 border rounded-lg flex justify-between items-center gap-4'>
                                <div className='font-bold'>{mem.name}</div>

                                {mem.is_active ?
                                    <input type='number' className="form-input rounded-lg" maxLength={12} id="name"
                                        onChange={e => setUpdateChangeSt(
                                            updateChangeSt => {
                                                if (updateChangeSt.some(item => item.member_id === mem.id)) {
                                                    return updateChangeSt.map(item => {
                                                        if (item.member_id === mem.id) item.amount = parseInt(e.target.value)
                                                        return item
                                                    })
                                                } return [...updateChangeSt, { member_id: mem.id, amount: parseInt(e.target.value) }]
                                            }

                                        )}
                                    ></input>
                                    : <Currentcy balance={mem.balance} />}
                            </div>)}
                    </div>
                </div>

                <div className='text-center'>
                    <button className="btn btn-danger" onClick={handleUpdateBalance}>Save</button>
                </div>
            </Modal>
        </>
    )
}