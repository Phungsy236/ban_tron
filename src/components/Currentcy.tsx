
type Props = {
    balance: number
}

export default function Currentcy({ balance }: Props) {
    const formattedNumber = balance.toLocaleString('vi-VN', { style: 'currency', currency: 'VND' });
    const color = balance >= 0 ? 'text-green-500' : 'text-red-500'
    return (
        <div className={'font-bold '+color}>{formattedNumber}</div>
    )
}